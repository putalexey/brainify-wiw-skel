$(document).ready(function() {
    window.removeExistingPicture = function (element, field_name) {
        var $form = $(element).closest('form');
        deleteImage($form.data('code'), $(element).parent().data('id'));
        $(element).closest('.dropzone-previews').find('.uploaded-form-single').show();
        $(element).closest('.upload-image').remove();

        $form.trigger('image.removed', {
            field_name: field_name
        });
    };
    function deleteImage(code, old_image_id, callback) {
        // BR.WIW.abortRPC();
        BR.WIW.__sendJsonRpc(
            code + ".photoDelete",
            {photo_id: old_image_id},
            {success: callback || $.noop},
            false
        );
    }
    $('.uploaded-form,.uploaded-form-single').each(function() {
        var $container = $(this);
        var $form = $container.closest('form');
        var $input = $container.find('input[type="file"]');
        var field_name = $input.attr('name').replace('[]', '');
        var singleFile = !$input.attr('multiple');
        var previewsContainerSelector = $input.data('preview-container') || '.uploaded-images';
        var clickableSelector = $input.data('clickable') || '.uploaded-form__label';
        var dropZone = new Dropzone($container.get(0), {
            url: window.location.href,
            paramName: $input.attr('name'),
            maxFiles: singleFile ? 1 : null,
            acceptedFiles: 'image/*',
            params: {
                AJAX: 'Y',
                sessid: BX.bitrix_sessid(),
                answer_id: $form[0]['fields[answer_id]'].value,
                form: $form.data('code')
            },
            thumbnailWidth: singleFile ? 604 : 187,
            thumbnailHeight: singleFile ? 378 : 133,
            clickable: clickableSelector,
            previewsContainer: previewsContainerSelector,
            previewTemplate: '<div class="upload-image">' +
            '<img data-dz-thumbnail>' +
            '<a href="#" data-dz-remove class="upload-image__delete">Удалить</a>' +
            '</div>'
        });
        dropZone.on('thumbnail', function (file, imageURI) {
            if (singleFile)
                $container.hide();
            $(file.previewElement).addClass('loading');

            // move form to end of preview container...
            $(file.previewElement).parent().find('.uploaded-form').appendTo($(file.previewElement).parent());
        });
        dropZone.on('success', function (file, response) {
            $(file.previewElement).removeClass('loading');
            var $img = $(file.previewElement).find('img');
            // $img.parent().data('id', response.files[0].answer_value_id);
            $img.parent().data('id', response[field_name][0].answer_value_id);
            $form.trigger('image.uploaded', {
                field_name: field_name,
                response: response,
                input: $input,
            });
        });
        dropZone.on('removedfile', function (file) {
            deleteImage($(file.previewElement).closest('form').data('code'), $(file.previewElement).data('id'));
            if (singleFile)
                $container.show();

            $form.trigger('image.removed', {
                field_name: field_name
            });
        });
    });
});
$(function() {
    var animation = {
            steps: 25, // animation tick timeout
            timeout: 20, // animation tick timeout
        },
        pie = {
            pos: { // pos of the pie
                x: 25,
                y: 25,
            },
            radius: 25 // radius of the pie
        };

    BR.animatePercents = function($parent, firstTimeout) {
        if (typeof firstTimeout === 'undefined')
            firstTimeout = 1000;
        $parent.find(".percent-pie").each(function () {
            if ($(this).data('percent-animated')) return;
            $(this).data('percent-animated', true);

            var target = $(this).data('radius'), // нужный угол
                alpha = 0, // текущий угол
                // pie = $(this).find('.pie'), // изменяемый элемент
                // changed to [class~=pie] due to jquery from 1.11.2 not checking
                // existence of getElementsByClassname in context before applying
                // getElementsByClassname https://github.com/jquery/sizzle/issues/322
                // https://github.com/jquery/sizzle/commit/08cb4519b079af502e0441857bc3c01d31030545
                pie = $(this).find('[class~=pie]'), // изменяемый элемент
                d = (target - alpha) / animation.steps; // изменение угла за 1 шаг
            if (target > 0) {
                setTimeout(function tickDraw() {
                    drawPie(alpha, pie, d);
                    if (target - alpha > 0.000000001) {
                        alpha += d;
                        setTimeout(tickDraw, animation.timeout);
                    }
                }, firstTimeout);
            }
        });
    };
    BR.animatePercents($('.header__section_current'), 1000);

    function drawPie(alpha, loader) {
        if (alpha > 360)
            alpha = 360;
        var r = ( alpha * Math.PI / 180 )
            , x1 = Math.sin(r / 2) * pie.radius + pie.pos.x
            , y1 = Math.cos(r / 2) * -pie.radius + pie.pos.y
            , x2 = Math.sin(r) * pie.radius + pie.pos.x
            , y2 = Math.cos(r) * -pie.radius + pie.pos.y
            , mid = 0//( alpha > 180 ) ? 1 : 0
            , anim = 'M ' + pie.pos.x + ' ' + pie.pos.y
            + ' v -' + pie.radius
            + ' A ' + pie.radius + ' ' + pie.radius + ' 1 '
            + mid + ' 1 '
            + x1 + ' '
            + y1
            // + ' M ' + x1 + ' ' + y1
            + ' A ' + pie.radius + ' ' + pie.radius + ' 1 '
            + mid + ' 1 '
            + x2 + ' '
            + y2
            + ' z';
        loader.attr('d', anim);

        return alpha;
    }
});
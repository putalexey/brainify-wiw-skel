$(document).ready(function(){
    // admin panel css bug...
    if ($('#panel').children().length) {
        $('.header-wrap').css('marginTop', '40px');
    }

    //маска на все инпуты для телефона
    $('input[type=tel]').mask('+9 ( 999 ) 999 - 99 - 99');

    //плавный скроллинг по ссылкам-якорям
    $('a.js-anchor').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
            && location.hostname == this.hostname) {
            var $target = $(this.hash);
            $target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
            if ($target.length) {
                var targetOffset = $target.offset().top;
                $('html,body').animate({scrollTop: targetOffset}, 600);//скорость прокрутки
                return false;
            }
        }
    });

    //список секций упражнения в хедере
    $('.header__section_current').click(function(e){
        var $dropSections = $('.header__drop-sections'),
            $curSection = $('.header__section_current');
        if ($(this).parent().attr('class') == 'header__sections') {
            e.preventDefault();
            $dropSections.addClass('header__drop-sections_is-open');
            $curSection.addClass('header__section_current-open');
            BR.animatePercents($('.header__section').not(".header__section_current"), 0);
        } else {
            $dropSections.removeClass('header__drop-sections_is-open');
            $curSection.removeClass('header__section_current-open');
        }
    });

    //если список секций в хедере открыт, то при клике в любую область документа (кроме самого списка) скрываем его
    $(document).click(function(e){
        var $sections = $('.header__sections'),
            $dropSections = $('.header__drop-sections'),
            $curSection = $('.header__section_current');
        if (!$sections.is(e.target) && $sections.has(e.target).length === 0 && $dropSections.has('.header__drop-sections_is-open')) {
            $dropSections.removeClass('header__drop-sections_is-open');
            $curSection.removeClass('header__section_current-open');
        }
    });

    //открыть профиль
    $('.header__humburger').click(function(){
        var $profile = $('.header__profile');
        if ($(this).parent().attr('class') == 'header__personal') {
            $profile.addClass('header__profile_is-open');
        } else {
            $profile.removeClass('header__profile_is-open');
        }
    });

    //если профиль в хедере открыт, то при клике в любую область документа (кроме самого профиля) скрываем его
    $(document).click(function(e){
        var $personal = $('.header__personal'),
            $profile = $('.header__profile');
        if (!$personal.is(e.target) && $personal.has(e.target).length === 0 && $profile.has('.header__profile_is-open')) {
            $profile.removeClass('header__profile_is-open');
        }
    });

    //отображение нижнего меню, если пользователь скроллит вверх
    var $footer = $('.footer-wrap');
    $(window).on('touchstart', function(e) {
        var swipe = e.originalEvent.touches,
            start = swipe[0].pageY;
        $(this).on('touchmove', function (e) {
            var contact = e.originalEvent.touches,
                end = contact[0].pageY,
                distance = end - start,
                direction;

            if (distance < -30) // up
            {
                direction = 'down';
            }
            if (distance > 30) // down
            {
                direction = 'up';
            }

            if ((direction === 'down' || $(window).scrollTop() === 0) && ($(window).scrollTop() !== $(document).height() - $(window).height())) {
                $footer.removeClass('footer-wrap_displayed');
            } else if (direction === 'up') {
                $footer.addClass('footer-wrap_displayed');
            }
        }).one('touchend', function () {
            $(this).off('touchmove touchend');
        });
    });
    WheelIndicator({
        callback: function(e){
            //если скроллим вниз или достигли самого верха, то скрываем, но при этом если пытаемся скроллить вниз в самом низу страницы, то не скрываем %)
            if ((e.direction == 'down' || $(window).scrollTop() == 0) && ($(window).scrollTop() != $(document).height() - $(window).height())) {
                $footer.removeClass('footer-wrap_displayed');
            } else if (e.direction == 'up') {
                $footer.addClass('footer-wrap_displayed');
            }
        },
        preventMouse: false
    });

    //при скролле проверяем и устанавлием состояние для основного контента, хедера и футера
    contentCheck();
    footerCheck();
    headerCheck();
    $(window).scroll(function() {
        contentCheck();
        footerCheck();
        headerCheck();
    });

    //открыть следующие шаги
    $('.step__button-next').click(function(){
        var steps = $(this).data('steps'),
            body = $("html, body"),
            $textarea = $('.step__textarea'),
            scroll = $(window).scrollTop(),
            $steps = $('.step');
        if (BX.util.isNumber(steps)) {
            steps = [steps];
        } else {
            steps = steps.split(',');
        }
        if (openStep(steps)) {
            $footer.removeClass('footer-wrap_displayed');
            BR.updateElementExpanding($textarea);
            makeMultilinePlaceholders();
            // $(this).data('steps', 'disabled'); //делаем кнопку неактивной

            //если шаги не пустые и кнопка активна
            if (steps.length && steps[0] != 'disabled' && $steps.eq(steps[0] - 1).length) {
                scroll = $steps.eq(steps[0] - 1).offset().top - 110;
            }

            body.stop().animate({scrollTop: scroll}, '1000', 'swing'); //проматываем страницу чуть ниже
        }
    });

    //при клике на сопровождение по почте ставим такое же состояние в другом месте
    $('.switch__checkbox').change(function(){
        var state = $(this).is(':checked'),
            $checkboxes = $('.switch__checkbox');
        if (!state) {
            $checkboxes.prop('checked', false);
        } else {
            $checkboxes.prop('checked', true);
        }
    });

    //пока копирование только для одной кнопки на странице
    if ($('.step__button-copy').length) {
        var button = document.getElementsByClassName('step__button-copy')[0];
        var input = document.getElementsByClassName('step__textarea')[0];
        var supported = document.queryCommandSupported("copy");
        if (supported) {
            //проверяем, поддерживает ли браузер метод
            try {
                button.addEventListener("click", function(event) {
                    event.preventDefault();
                    input.select();
                    document.execCommand("copy");
                });
            } catch (e) {
                supported = false;
            }
        }
        if (!supported) {
            button.style.display='none'
        }
    }

    //открыть блок с подсказкой
    $('.step__hidden-block-title').click(function(e){
        e.preventDefault();
        var $block = $(this).next('.step__hidden-block');
        if ($block.is(':hidden')){
            $block.fadeIn();
        } else {
            $block.fadeOut();
        }
    });

    //добавление/удаление связанных инпутов
    $('body').on('click', '.constructor-list__control', function(){
        var $constructor = $('.constructor-list'),
            $constructed = $('.constructed-list'),
            index = $(this).parent().index(),
            classes = $constructed.attr('class').split(' ');
        if (!$(this).hasClass('constructor-list__control_minus')) { //добавление
            var target = $(this).parent().find('.step__textarea,.step__text-input'),
                type = target.hasClass('.step__textarea') ? 'textarea' : 'input';
            add2Constructor($constructor, target, type);
            if (classes.length == 1) { //в стандартный список
                add2Constructed($constructed, $constructed.find('.constructed-list__item:eq(' + index + ') .step__textarea'));
            } else if (classes[1] == 'constructed-list_double') { //в список с двойной текстовой областью
                add2DoubleConstructed($constructed, $constructed.find('.constructed-list__item:eq(' + index + ') .step__textarea'));
            }
            $('.constructor-list__item:not(:last)')
                .find('.constructor-list__control')
                .addClass('constructor-list__control_minus')
                .text('-'); //всем предыдущие инпуты делаем на удаление, а не на добавление
            var $textarea = $('.step__textarea');
            $textarea.length && BR.updateElementExpanding($textarea);
            makeMultilinePlaceholders();
        } else { //удаление
            delElement($constructor, index);
            delElement($constructed, index);
        }
    });
    $('.constructor-list.js-add-onenter').on('keypress', 'input[type="text"]', function(e) {
        if (e.which === 13) {
            e.preventDefault();
            var element = $(e.target).closest('.constructor-list__item'),
                allElements = element.parent().children('.constructor-list__item');
            if (element.index() === allElements.length - 1) {
                element.find('.constructor-list__control').click();
            }

            element.next().find('input[type="text"]').focus();
        }
    });

    //изменение лэйбла в создаваемом списке в зависимости от инпутов
    //при загрузке страницы
    $.each($('.constructor-list__item'), function(i, item) {
        var $constructed = $('.constructed-list'),
            value = $(item).find('.step__textarea,.step__text-input').val();
            changeLabel($constructed, i, value);
    });
    //при изменении инпута
    $('body').on('keyup', '.constructor-list__item .step__textarea,.constructor-list__item .step__text-input', function(){
        var $constructed = $('.constructed-list'),
            value = $(this).val(),
            index = $(this).closest('.constructor-list__item').index();
        changeLabel($constructed, index, value);
    });

    //режим HADI/SMART в упражнении "Цель"
    if ($('#hadi').is(':checked')) {
        var $title = $('.js-hadi-title'),
            $smart = $('.js-smart'),
            $hadi = $('.js-hadi');
        $title.text('Мои возможные цели:');
        $smart.hide();
        $hadi.show();
    }
    $('#hadi').change(function(){
        var checked = $(this).is(':checked'), //true - HADI, false - SMART
            $title = $('.js-hadi-title'),
            $smart = $('.js-smart'),
            $hadi = $('.js-hadi'),
            $hiddenStep = $('.step__hadi'),
            nextButton = $('.step__button-next').data('steps');
        if (checked) {
            $title.text('Мои возможные цели:');
            $smart.hide();
            $hadi.show();
            if (nextButton == 'disabled')
                $hiddenStep.show();
        } else {
            $title.text('Моя цель:');
            $hadi.hide();
            $smart.show();
            $hiddenStep.hide();
        }
        BR.updateElementExpanding($smart.add($hadi).add($hiddenStep).find('textarea.expanding'));
    });

    //выбор способа порадовать в упражнении "Спонтанный акт доброты"
    $('.way2please__item').click(function(){
        var $items = $('.way2please__item'),
            $input = $('.way2please__input'),
            index = $(this).index(),
            checked = $(this).find('input').prop('checked');

        if (checked) {
            $(this).addClass('way2please__item_active');
            if (index == $items.length - 1) { //последний элемент: "придумаю сам" открывает текстовый инпут
                $input.show();
            }
        } else {
            $(this).removeClass('way2please__item_active');
            if (index == $items.length - 1) { //последний элемент: "придумаю сам" скрывает текстовый инпут
                $input.hide();
            }
        }
    });

    //инициализируем набор цветов
    if ($('.colorpicker__item').length) {
        $.each($('.colorpicker__item'), function(i, item){
            var color = $(item).data('color');
            $(item).css('background', color);
        });
        $('.colorpicker__item').click(function(){
            var $items = $('.colorpicker__item');
            $items.removeClass('colorpicker__item_active');
            $(this).addClass('colorpicker__item_active');
        });
    }

    //открыть блок с подсказкой упражнении 360
    $('.questionnaire__title').click(function(e){
        e.preventDefault();
        var $block = $('.questionnaire'),
            $title = $('.questionnaire__title');
        if ($block.is(':hidden')){
            $block.fadeIn();
            $title.text('скрыть');
        } else {
            $block.fadeOut();
            $title.text('открыть');
        }
    });

    //блок поделиться в упражнении 360
    $('.share-360__link').click(function(){
        var index = $(this).index(),
            $tabs = $('.share-360__tab'),
            $links = $('.share-360__link'),
            $textarea = $('.step__textarea');
        $links.addClass('border-bottom');
        $(this).removeClass('border-bottom');
        $tabs.hide();
        $tabs.eq(index).show();
        BR.updateElementExpanding($textarea);
        makeMultilinePlaceholders();
    });

    //фильтр по ответам в упражнении 360
    $('.answers').on('click', '.answers__filter-item', function(){
        var $filterItems = $('.answers__filter-item'),
            filter = $(this).data('filter'),
            $blocks = $('.answers__block'),
            $allAnswers = $('.answers__item');
        $filterItems.addClass('border-bottom');
        $(this).removeClass('border-bottom');
        if (filter == 'all') {
            $allAnswers.show(); //показываем все ответы
            $blocks.show(); //и блоки
        } else {
            $allAnswers.hide(); //сначала скрываем все ответы
            $.each($blocks, function(i, block){
                var $answers = $(block).find($allAnswers),
                    empty = true; //считаем, что блок пустой, т.к. все ответы скрыты
                $.each($answers, function(i, answer){
                    if ($(answer).data('answer') == filter) {
                        $(answer).show();
                        empty = false; //если нашли ответ, то блок не пустой
                    }
                });
                if (empty)
                    $(block).hide();
                else
                    $(block).show();
            });
        }
    });

    //табы в дневниках
    $('ul.tabs').on('click', 'li:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('div.wrap-tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    }); 


    //моменты вдохновения - пример
    $('.inspiration-example__title span').click(function(){
        var $block = $('.inspiration-example .step__hint');
        if ($block.is(':hidden')) {
            $block.fadeIn();
            $(this).text('свернуть');
        } else {
            $block.fadeOut();
            $(this).text('развернуть');
        }
    });

    //моменты вдохновения - советы
    $('.inspiration-advice span').click(function(){
        var $block = $('.inspiration-advice .step__hint');
        if ($block.is(':hidden')) {
            $block.fadeIn();
            $(this).removeClass('border-bottom');
        } else {
            $block.fadeOut();
            $(this).addClass('border-bottom');
        }
    });

    //добавить тег в облако тегов
    $('.cloudarea').keydown(function(e){
        //нажатие на enter
        if (e.which === 13) {
            e.preventDefault();
            var values = $(this).val().split("\n"), //значения из текстовой области
                fieldValue = [], // новое значение поля журнала
                valIndex, //здесь будет индекс совпадающих значений
                $cloud = $('.tag-cloud').not('.tag-cloud_hidden'), //видимое облако тегов
                $cloudItem = $cloud.find('.tag-cloud__item'),
                $input = $cloud.find('.tag-cloud__input'), //скрытый инпут
                cloudItemsValues = getCloudItems($cloud); //значения из облака тегов
            if ($cloud.length > 1 || $input.length > 1) {
                BR.SendError(null, 'PageError', 'Показано слишком много облаков тэгов журнала, возможно ошибка на странице');
                throw new Exception('Показано слишком много тэгов, возможно ошибка на странице');
            }
            for (var i = 0; i < values.length; i++) {
                var curVal = $.trim(values[i]);
                if (curVal == '') //пустые строки
                    continue;
                if ((valIndex = in_array(curVal, cloudItemsValues)) !== false) { //если нашли индекс
                    if (!$cloudItem.eq(valIndex).hasClass('tag-cloud__item_active')) { //и ещё не меняли тег
                        $cloudItem.eq(valIndex).addClass('tag-cloud__item_active');
                        $cloudItem.eq(valIndex).data('weight', $cloudItem.eq(valIndex).data('weight') + 1); //увеличиваем вес на 1
                        $cloudItem.eq(valIndex).attr('data-weight', $cloudItem.eq(valIndex).data('weight')); //для отображения в интерфейсе
                        $input.val($input.val() + curVal + ';');
                    }
                } else { // добавляем новый тег
                    $cloud.append('<div class="tag-cloud__item tag-cloud__item_active" data-weight="1">' + curVal + '<span class="tag-cloud__icon"></span></div>');
                    $input.val($input.val() + curVal + ';');
                }
            }
            $(this).val(''); //убираем значение из области
            $('.cloudarea').change(); //так область принимает исходную высоту
            $cloud.trigger('wiw.save-form');
        }
    });

    //переключение облаков с тегами в секции "предпочтения"
    $('.tag-marks__item').click(function(){
        var index = $(this).index();
        $('.tag-marks__item').removeClass('tag-marks__item_active');
        $(this).addClass('tag-marks__item_active');
        $('.tag-cloud').addClass('tag-cloud_hidden');
        $('.tag-cloud').eq(index).removeClass('tag-cloud_hidden');
    });

    //удалить из облака тегов
    $('body').on('click', '.tag-cloud__icon', function() {
        var $item = $(this).parent(),
            $cloud = $item.closest('form'),
            $inputs = $cloud.find('.tag-cloud__input'), //скрытый инпут
            value = $.trim($item.text()),
            removed = false;

        // выберем input автивного раздела
        $inputs = $item.parent().find('.tag-cloud__input');
        if ($item.hasClass('tag-cloud__item_active') || $item.attr('data-remove-only') !== undefined) {
            if ($item.data('weight') == 1) {
                $item.remove();
            } else {
                $item.removeClass('tag-cloud__item_active');
                $item.data('weight', $item.data('weight') - 1); //уменьшаем вес на 1
                $item.attr('data-weight', $item.data('weight')); //для отображения в интерфейсе
            }
            $inputs.each(function () {
                if (!removed) {
                    var $input = $(this),
                        old_value = $input.val(),
                        new_value;
                    if (old_value.indexOf(value + ';') !== -1) {
                        new_value = old_value.replace(value + ';', '');
                        $input.val(new_value);
                        $cloud.trigger('wiw.save-form');
                        removed = true;
                    }
                }
            });
        } else {
            // add 1 tag
            $item.addClass('tag-cloud__item_active');
            $item.data('weight', $item.data('weight') + 1); //уменьшаем вес на 1
            $item.attr('data-weight', $item.data('weight')); //для отображения в интерфейсе

            $inputs.val($inputs.val() + value + ";");
            $cloud.trigger('wiw.save-form');
        }
    });

    //высота текстовой области с плейсхолдером в несколько строк
    if ($('.multiline-placeholder-wrap').length) {
        makeMultilinePlaceholders();
    }

    //список секций упражнения в Дневнике
    $('.archive__parent-link .arrow').click(function(e){
        e.preventDefault();

        $(this).parent().parent().toggleClass('archive-active').find('.archive__child').slideToggle(200);
    });
    // hide on load
    $('.archive .archive__child.hide_on_load').hide();

    $('.diary .archive a').not('.archive__parent-link').each(function() {
        var $link = $(this),
            href = $link.attr('href'),
            $target;
        if (href.indexOf('#') === 0) {
            $target = $(href);
            if ($target.length) {
                $link.click(function (e) {
                    e.preventDefault();
                    var targetOffset = $target.offset().top;
                    $('html,body').animate({scrollTop: targetOffset - 100}, 600);//скорость прокрутки
                });
            }
        }
    });


    //Липкие архивы
    var windowTop = $(window).scrollTop();
    var blockTop = $("#overall-tasks").offset().top;
    var fromTop = 70;
    if(windowTop > blockTop - fromTop){
    
         $("#overall-tasks").css("margin-top", windowTop - blockTop + fromTop);
    }else{
        $("#overall-tasks").css("margin-top", 0);
    }

    $('.email-support__tooltip').each(function() {
        function updateTooltipPosition() {
            var tooltip_width = $tooltip.outerWidth(),
                question_offset = $tooltip_question.offset() + $tooltip_question.outerWidth() / 2,
                question_center = question_offset.left + $tooltip_question.outerWidth() / 2,
                window_width = $(window).width(),
                new_pos = {
                    left: Math.min(
                        window_width - tooltip_width,
                        question_center - tooltip_width / 2
                    ),
                    top: question_offset.top + $tooltip_question.outerHeight() + 32
                };
            $tooltip.css(new_pos);
        }
        var $tooltip = $(this),
            $tooltip_question = $(this).closest('.email-support__question'),
            hover_timer = 0;
        // move to the root element
        $tooltip.appendTo($('body'));

        $tooltip_question.add($tooltip).hover(function() {
            clearTimeout(hover_timer);
            $tooltip.css({
                'visibility': 'visible',
                'opacity': 1
            });
        }, function () {
            clearTimeout(hover_timer);
            hover_timer = setTimeout(function() {
                $tooltip.css({
                    'visibility': 'hidden',
                    'opacity': 0
                });
            }, 200);
        });

        $(window).resize(updateTooltipPosition);
        updateTooltipPosition();
    });
});












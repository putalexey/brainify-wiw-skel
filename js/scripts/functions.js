if (!window.BX) window.BX = {util: {isNumber: function(n){ if (n===undefined || n===null) return false; return n.constructor===Number; }}}
if (!window.BR) window.BR = { updateElementExpanding: function () {}}
function openStep(steps) {
    var $steps = $('.step');
    //если шаги не пустые и кнопка активна
    if (steps.length && steps[0] != 'disabled') {
        steps.forEach(function(step) {
            $steps.eq(step - 1).fadeIn(1100);
        });
        return true;
    }
}

function isVisible(tag) {
    var $elem = $(tag);
    var $window = $(window);
    var wh = $window.height();
    var wt = $window.scrollTop();
    var et = $elem.offset().top;
    return (et <= wt + wh);
}

function contentCheck() {
    var $content = $('.content');
    if (!$content.prop("shown") && isVisible($content)) {
        $content.prop("shown", true);
        $content.addClass('content_displayed');
        setTimeout(function(){
            BR.updateElementExpanding($content.find('textarea.expanding'));
        }, 300);
    }
}

function footerCheck() {
    var $footer = $('.footer-wrap');
    if  ($(window).scrollTop() == $(document).height() - $(window).height()) {
        $footer.addClass('footer-wrap_displayed');
    } else if ($(window).scrollTop() == 0) {
        $footer.removeClass('footer-wrap_displayed');
    }
}

function headerCheck() {
    var $header = $('.header-wrap');
    if  ($(window).scrollTop() == 0) {
        $header.prop('shadow', false);
        $header.removeClass('header-wrap_shadow');
    } else if (!$header.prop('shadow') && $(window).scrollTop() > 0) {
        $header.prop('shadow', true);
        $header.addClass('header-wrap_shadow');
    }
}

function add2Constructor(tag, textareas, type) {
    var $elem = $(tag);
    if (type == "input") {
        $elem.append('<li class="constructor-list__item">' +
             //'<input type="text" class="step__text-input" placeholder="Начни писать здесь">' +
             '<input type="text" class="step__text-input" placeholder="Начни писать здесь" name="' + textareas.eq(0).attr('name') + '">' +
             '<span class="constructor-list__control">+</span>' +
             '</li>'
            );
    } else {
        $elem.append('<li class="constructor-list__item">' +
             //'<input type="text" class="step__text-input" placeholder="Начни писать здесь">' +
             '<textarea class="step__textarea expanding" placeholder="Начни писать здесь" name="' + textareas.eq(0).attr('name') + '"></textarea>' +
             '<span class="constructor-list__control">+</span>' +
             '</li>'
            );
    }
}

function add2Constructed(tag, textareas) {
    var $elem = $(tag);
    $elem.append('<li class="constructed-list__item">' +
                 '<label class="constructed-list__label"></label>' +
                 '<textarea class="step__textarea expanding" placeholder="Начни писать здесь" name="' + textareas.eq(0).attr('name') + '"></textarea>' +
                 '</li>'
    );
}

function add2DoubleConstructed(tag, textareas) {
    var $elem = $(tag);
    $elem.append('<li class="constructed-list__item item__hidden">' +
        '<label class="constructed-list__label"></label>' +
        '<textarea class="step__textarea expanding" placeholder="Меня восхищает" name="' + textareas.eq(0).attr('name') + '"></textarea>' +
        '<textarea class="step__textarea step__textarea_second expanding" placeholder="В чем изюминка?" name="' + textareas.eq(1).attr('name') + '"></textarea>' +
        '</li>'
    );
}

function delElement(tag, index) {
    var $elem = $(tag);
    $elem.children().eq(index).remove();
}

function changeLabel(tag, index, val) {
    var $elem = $(tag);
    var $target = $elem.children().eq(index);
    $target.find('label').html(BX.util.htmlspecialchars(val).replace(/\r?\n/g, "<br>"));

    if(val.length) {
        $target.show();
        BR.updateElementExpanding($target.find('textarea.expanding'));
    }else {
        $target.hide();
    }
}

function getCloudItems(cloud) {
    var cloudItems = $(cloud).find('.tag-cloud__item'),
        items = [];
    $.each(cloudItems, function(index, item){
        var text = $.trim($(item).text());
        items.push(text.toLowerCase());
    });
    return items;
}

function in_array(value, array) {
    for(var i = 0; i < array.length; i++) {
        if(array[i] == value.toLowerCase()) return i;
    }
    return false;
}

function makeMultilinePlaceholders() {
    $.each($('.multiline-placeholder-wrap'), function(i, item) {
        if (!$(item).hasClass('multiline-placeholder-wrap__active') && $(item).is(':visible')) {
            var $area = $(item).find('textarea'),
                text = $area.attr('placeholder');
            $(item).append('<div class="multiline-placeholder">' + text + '</div>');
            var $ph = $(item).find('.multiline-placeholder'),
                height = $ph.height();
            $area.parent().css('min-height', height + 10); // 10 = padding
            $ph.remove();
            $(item).addClass('multiline-placeholder-wrap__active');
        }
    });
}





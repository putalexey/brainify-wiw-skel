if(typeof BR == 'undefined') BR={};

// Extend BX.util with additional type checks
(function(){
    // by Glen R. Goodwin (https://github.com/arei)
    // https://github.com/arei/util-is/blob/master/util-is.js
    var Util = BX.util || {};
    var toString = Object.prototype.toString;

    /*
     isString(s) Returns true if the passed in object (o) is a String.
     */
    Util.isString = function(s) {
        if (s===undefined || s===null) return false;
        return s.constructor===String;
    };

    /*
     isFunction(s) Returns true if the passed in object (o) is a Function.
     */
    Util.isFunction = function(f) {
        if (f===undefined || f===null) return false;
        return f.constructor===Function;
    };

    /*
     isNumber(s) Returns true if the passed in object (o) is a Number, Infinity,
     or NaN.  Any Number type, integer, float, etc, will return true;
     */
    Util.isNumber = function(n) {
        if (n===undefined || n===null) return false;
        return n.constructor===Number;
    };

    /*
     isBoolean(s) Returns true if the passed in object (o) is a Boolean,
     true, or false.
     */
    Util.isBoolean = function(b) {
        if (b===undefined || b===null) return false;
        return b.constructor===Boolean;
    };

    /*
     isDefined(s) Returns true if the passed in object (o) is not undefined.
     */
    Util.isDefined = function(o) {
        return o!==undefined;
    };

    /*
     isEmpty(s) Returns true if the passed in object (o) is a undefined,
     null, an empty array, or an empty string.  0 and false are not technically
     empty, so those values return true;
     */
    Util.isEmpty = function(o) {
        if (o===undefined || o===null) return true;
        if (jQuery.isArray(o) && o.length===0) return true;
        if (Util.isString(o) && o.length===0) return true;
        if (Util.isPureObject(o) && Object.keys(o).length===0) return true;
        return false;
    };

    /*
     isUndefined(s) Returns true if the passed in object (o) is not undefined.
     */
    Util.isUndefined = function(o) {
        return o===undefined;
    };

    /*
     isObject(s) Returns true if the passed in object (o) is a Object, which
     is everything that is not undefined.
     */
    Util.isObject = function(o) {
        return o!==undefined;
    };

    /*
     isPureObject(o) Returns true if the passed in object (o) is defined
     and not a function, string, number, boolean, array, date, RegExp, or Error.
     */
    Util.isPureObject = function(o) {
        return o!==undefined &&
            o!==null &&
            !Util.isFunction(o) &&
            !Util.isString(o) &&
            !Util.isNumber(o) &&
            !Util.isBoolean(o) &&
            !Util.isArray(o)  &&
            !Util.isDate(o) &&
            !Util.isRegExp(o) &&
            !Util.isError(o);
    };

    /*
     getPlural(count, forms) Return plural version of word
     */
    Util.getPlural = function(count, forms) {
        var tens = count % 100;
        var ones = tens % 10;

        if(ones == 1 && tens != 11)
            return forms[0];
        if(ones >= 2 && ones <= 4 && (tens < 10 || tens >= 20))
            return forms[1];
        return forms[2];
    };
})();
(function($, document, window){
    BR.SendError = function(xhr, status, error) {
        var data = {
            sessid: BX.bitrix_sessid(),
            status: status,
            message: error,
            page: window.location.href
        };
        data.text = xhr && xhr.responseText || '';

        if (typeof status !== "string")
            status = JSON.stringify(status);
        if (typeof error !== "string")
            error = JSON.stringify(error);

        // var url = '/api/sendError.php?status=' + encodeURIComponent(status) + '&message=' + encodeURIComponent(error);
        var url = '/api/sendError.php';

        $.ajax({
            url: url,
            method: 'post',
            data: data
        });
    };
    BR.updateExpanding = function(block) {
        if(!block.jquery) block = $(block);
        BR.updateElementExpanding(block.find('textarea.expand'));
    };
    BR.updateElementExpanding = function(element) {
        if(!element.jquery) element = $(element);
        element.each(function() {
            var $this = $(this);
            if($this.is(':visible')) {
                if (!$this.expanding('active')) {
                    $this.expanding();
                }
            }
        });
    };
    BR.removeExpanding = function(block) {
        if(!block.jquery) block = $(block);
        block.find('textarea.expand').each(function() {
            var $this = $(this);
            if ($this.expanding('active'))
                $this.expanding('destroy');
        });
    };
    /*BR.setHoverFadeIn = function(hoverElement, showElement){
        hoverElement.hover(
            function(){
                showElement.stop(true).fadeIn();
            },
            function(){
                showElement.fadeOut();
            }
        );
    };*/
    String.prototype.replaceArray = function(find, replace) {
        var replaceString = this;
        for (var i = 0; i < find.length; i++) {
            replaceString = replaceString.replace(find[i], replace[i]);
        }
        return replaceString;
    };

    BR.POPUPS = {};

    if(typeof BX != "undefined" && BX.CWindowDialog)
    {
        // zindex < 10000000 overlapped by slaask
        if(BX.WindowManager) BX.WindowManager._delta_start = 10000002;
        /* BR.Dialog class based on BX.CDialog */
        BR.Dialog = function(arParams)
        {
            BR.Dialog.superclass.constructor.apply(this);
            this._sender = 'core_window_br_dialog';

            this.PARAMS = arParams || {};

            for (var i in this.defaultParams)
            {
                if (typeof this.PARAMS[i] == 'undefined')
                    this.PARAMS[i] = this.defaultParams[i];
            }

            if(this.PARAMS.width) this.PARAMS.max_width = this.PARAMS.width;
            //this.PARAMS.width = (!isNaN(parseInt(this.PARAMS.width)))
            //    ? this.PARAMS.width
            //    : this.defaultParams['width'];
        if(this.PARAMS.width && this.PARAMS.width < this.PARAMS.min_width)
            this.PARAMS.min_width = this.PARAMS.width;

        //if(this.PARAMS.height) this.PARAMS.max_height = this.PARAMS.height;
        //this.PARAMS.height = (!isNaN(parseInt(this.PARAMS.height)))
        //    ? this.PARAMS.height
        //    : this.defaultParams['height'];

            if (this.PARAMS.resize_id || this.PARAMS.content_url)
            {
                var arSize = BX.WindowManager.getRuntimeWindowSize(this.PARAMS.resize_id || this.PARAMS.content_url);
                if (arSize)
                {
                    this.PARAMS.width = arSize.width;
                    this.PARAMS.height = arSize.height;
                }
            }

            BX.addClass(this.DIV, 'br-dialog');
            this.DIV.id = 'br-window';

            this.PARTS = {};
            if(this.PARAMS.closable)
                this.PARTS.closeBtn = this.DIV.appendChild(BX.create('DIV', {
                    props: {className: 'br-dialog-close-btn'}
                }));
            this.DIV.style.height = null;
            this.DIV.style.width = null;
            this.PARTS.CONTENT = this.DIV.appendChild(BX.create('DIV', {
                props: {className: 'br-dialog-content-wrap'}
            }));

            this.PARTS.CONTENT_DATA = this.PARTS.CONTENT.appendChild(BX.create('DIV', {
                props: {className: 'br-dialog-content'}
        //        ,style: {
        //            height: this.PARAMS.height + 'px',
        //            width: this.PARAMS.width + 'px'
        //        }
            }));

            if (this.PARAMS.resizable)
            {
                this.PARTS.RESIZER = this.DIV.appendChild(BX.create('DIV', {
                    props: {className: 'br-resizer'}
                }));

                this.SetResize(this.PARTS.RESIZER);

                this.SETTINGS.min_width = this.PARAMS.min_width;
                this.SETTINGS.min_height = this.PARAMS.min_height;
            }

            if(this.PARAMS.buttons && this.PARAMS.buttons.length) {
                this.PARTS.FOOT = this.PARTS.BUTTONS_CONTAINER = this.PARTS.CONTENT.appendChild(BX.create('DIV', {
                        props: {
                            className: 'br-dialog-buttons'
                        },
                        children: this.ShowButtons()
                    }
                ));
            }

            var w = this.DIV.style.width;
            var h = this.DIV.style.height;
            this.SetSize({width: this.PARAMS.width || 400, height:this.PARAMS.height || 400});
            if(this.PARAMS.closable)
                this.SetClose(this.PARTS.closeBtn);
            else
                this.unclosable = true;
        };
        BX.extend(BR.Dialog, BX.CWindowDialog);

        BR.Dialog.prototype.defaultParams = {
            min_width: 320,
            min_height: 255,
            max_width: 700,
            max_height: 0,

            resizable: false,
            draggable: true,
            overlay_close: true,
            closable: true,

            title: '',
            icon: ''
        };

        //BR.Dialog.prototype.onClose = false;
        BR.Dialog.prototype.Close = function(){
            var self = this;
            BX.addClass(this.DIV, 'fadeOutDown animated');
            var closed = false;
            $(this.DIV).one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", closePopup);
            setTimeout(closePopup, 1000); // fallback if animationend event not worked (sometimes it happens)
            function closePopup(e) {
                if(closed) return;
                BX.removeClass(self.DIV, 'animated fadeOutDown');
                if(self.PARAMS.hideOnClose)
                {
                    BR.Dialog.superclass.Hide.apply(self);
                } else {
                    BR.Dialog.superclass.Close.apply(self);
                    for(var i in self.PARTS){
                        self.PARTS[i].remove();
                    }
                    self.DIV.remove();
                    if(self.onClose)
                        self.onClose();
                }
                closed = true;
            }
        };
        BR.Dialog.prototype.CreateOverlay = function(zIndex)
        {
            if (null == this.OVERLAY)
            {
                var windowSize = BX.GetWindowScrollSize();
                this.OVERLAY = document.body.appendChild(BX.create("DIV", {
                    style: {
                        position: 'fixed',
                        top: '0px',
                        left: '0px',
                        right: '0px',
                        bottom: '0px',
                        zIndex: zIndex || (parseInt(this.DIV.style.zIndex)-2)
                        //width: windowSize.scrollWidth + "px",
                        //height: windowSize.scrollHeight + "px"
                    }
                }));
            }

            return this.OVERLAY;
        };
        BR.Dialog.prototype.SetClose = function(elem)
        {
            BX.bind(elem, 'click', BX.proxy(this.Close, this));
            this.ELEMENTS.close.push(elem);
        };
        BR.Dialog.prototype.showWait = function(el)
        {
            if (BX.type.isElementNode(el) && (el.type == 'button' || el.type == 'submit'))
            {
                BX.defer(function(){el.disabled = true})();

                var bSave = (BX.hasClass(el, 'adm-btn-save') || BX.hasClass(el, 'adm-btn-save')),
                    pos = BX.pos(el, true);

                var container = BX.findParent(el, {className:this.PARTS.CONTENT_DATA.className}, this.PARTS.CONTENT_DATA);
                if(container == null)
                    container = this.PARTS.FOOT_DATA;

                el.bxwaiter = container.appendChild(BX.create('DIV', {
                    props: {className: 'br-btn-load-img'},
                    style: {
                        top: parseInt((pos.bottom + pos.top)/2 - 10) + 'px',
                        left: parseInt((pos.right + pos.left)/2 - 10) + 'px'
                    }
                }));

                BX.addClass(el, 'adm-btn-load');

                this.lastWaitElement = el;

                return el.bxwaiter;
            }
            return null;
        };

        BR.Dialog.prototype.closeWait = function(el)
        {
            el = el || this.lastWaitElement;

            if (BX.type.isElementNode(el))
            {
                if (el.bxwaiter)
                {
                    if(el.bxwaiter.parentNode)
                    {
                        el.bxwaiter.parentNode.removeChild(el.bxwaiter);
                    }

                    el.bxwaiter = null;
                }

                el.disabled = false;
                BX.removeClass(el, 'adm-btn-load');

                if (this.lastWaitElement == el)
                    this.lastWaitElement = null;
            }
        };

        BR.Dialog.prototype.ShowButtons = function()
        {
            var result = [];
            if (this.PARAMS.buttons)
            {
                if (this.PARAMS.buttons.title) this.PARAMS.buttons = [this.PARAMS.buttons];

                for (var i=0, len=this.PARAMS.buttons.length; i<len; i++)
                {
                    if (BX.type.isNotEmptyString(this.PARAMS.buttons[i]))
                    {
                        result.push(this.PARAMS.buttons[i]);
                    }
                    else if (this.PARAMS.buttons[i])
                    {
                        //if (!(this.PARAMS.buttons[i] instanceof BX.CWindowButton))
                        if (!BX.is_subclass_of(this.PARAMS.buttons[i], BX.CWindowButton))
                        {
                            var b = this._checkButton(this.PARAMS.buttons[i]); // hack to set links to real CWindowButton object in btnSave etc;
                            if(this.PARAMS.buttons[i].type && this.PARAMS.buttons[i].type == "link") {
                                this.PARAMS.buttons[i] = new BR.WindowLink(this.PARAMS.buttons[i]);
                            } else {
                                this.PARAMS.buttons[i] = new BX.CWindowButton(this.PARAMS.buttons[i]);
                            }
                            if (b) this[b] = this.PARAMS.buttons[i];
                        }

                        result.push(this.PARAMS.buttons[i].Button(this));
                    }
                }
            }

            return result;
        };
        BR.Dialog.prototype._checkButton = function(btn)
        {
            var arCustomButtons = ['btnSave', 'btnCancel', 'btnClose'];

            for (var i = 0; i < arCustomButtons.length; i++)
            {
                if (this[arCustomButtons[i]] && (btn == this[arCustomButtons[i]]))
                    return arCustomButtons[i];
            }

            return false;
        };

        BR.Dialog.prototype.ShowError = function(str)
        {
            BX.onCustomEvent(this, 'onWindowError', [str]);

            this.closeWait();

            if (this._wait)
                BX.closeWait(this._wait);

            this.Notify(str, true);
        };


        BR.Dialog.prototype.__expandGetSize = function()
        {
            var pDocElement = BX.GetDocElement();
            pDocElement.style.overflow = 'hidden';

            var wndSize = BX.GetWindowInnerSize();

            pDocElement.scrollTop = 0;

            this.DIV.style.top = '-' + this.dxShadow + 'px';
            this.DIV.style.left = '-' + this.dxShadow + 'px';

            return {
                width: (wndSize.innerWidth - parseInt(BX.style(this.PARTS.CONTENT, 'padding-right')) - parseInt(BX.style(this.PARTS.CONTENT, 'padding-left'))) + this.dxShadow,
                height: (wndSize.innerHeight - parseInt(BX.style(this.PARTS.CONTENT, 'padding-top')) - parseInt(BX.style(this.PARTS.CONTENT, 'padding-bottom'))) + this.dxShadow
            };
        };

        BR.Dialog.prototype.__expand = function()
        {
            var pDocElement = BX.GetDocElement();
            this.dxShadow = 2;

            if (!this.bExpanded)
            {
                var wndScroll = BX.GetWindowScrollPos();

                this.__expand_settings = {
                    resizable: this.SETTINGS.resizable,
                    draggable: this.SETTINGS.draggable,
                    width: this.PARTS.CONTENT_DATA.style.width,
                    height: this.PARTS.CONTENT_DATA.style.height,
                    left: this.DIV.style.left,
                    top: this.DIV.style.top,
                    scrollTop: wndScroll.scrollTop,
                    scrollLeft: wndScroll.scrollLeft,
                    overflow: BX.style(pDocElement, 'overflow')
                };

                this.SETTINGS.resizable = false;
                this.SETTINGS.draggable = false;

                var pos = this.__expandGetSize();

                this.PARTS.CONTENT_DATA.style.width = pos.width + 'px';
                this.PARTS.CONTENT_DATA.style.height = pos.height + 'px';

                window.scrollTo(0,0);
                pDocElement.style.overflow = 'hidden';

                this.bExpanded = true;

                BX.onCustomEvent(this, 'onWindowExpand');
                BX.onCustomEvent(this, 'onWindowResize');
                BX.onCustomEvent(this, 'onWindowResizeExt', [{'width': pos.width, 'height': pos.height}]);

                BX.bind(window, 'resize', BX.proxy(this.__expand_onresize, this));
            }
            else
            {
                BX.unbind(window, 'resize', BX.proxy(this.__expand_onresize, this));

                this.SETTINGS.resizable = this.__expand_settings.resizable;
                this.SETTINGS.draggable = this.__expand_settings.draggable;

                pDocElement.style.overflow = this.__expand_settings.overflow;

                this.DIV.style.top = this.__expand_settings.top;
                this.DIV.style.left = this.__expand_settings.left;
                this.PARTS.CONTENT_DATA.style.width = this.__expand_settings.width;
                this.PARTS.CONTENT_DATA.style.height = this.__expand_settings.height;
                window.scrollTo(this.__expand_settings.scrollLeft, this.__expand_settings.scrollTop);
                this.bExpanded = false;

                BX.onCustomEvent(this, 'onWindowNarrow');
                BX.onCustomEvent(this, 'onWindowResize');
                BX.onCustomEvent(this, 'onWindowResizeExt', [{'width': parseInt(this.__expand_settings.width), 'height': parseInt(this.__expand_settings.height)}]);
            }
        };

        BR.Dialog.prototype.__expand_onresize = function()
        {
            var pos = this.__expandGetSize();

            this.PARTS.CONTENT_DATA.style.width = pos.width + 'px';
            this.PARTS.CONTENT_DATA.style.height = pos.height + 'px';

            BX.onCustomEvent(this, 'onWindowResize');
            BX.onCustomEvent(this, 'onWindowResizeExt', [pos]);
        };

        BR.Dialog.prototype.__onexpand = function()
        {
        //    var ob = this.PARTS.TITLEBAR_ICONS.firstChild;
        //    ob.className = BX.toggle(ob.className, ['bx-core-adm-icon-expand', 'bx-core-adm-icon-narrow']);
        //    ob.title = BX.toggle(ob.title, [BX.message('JS_CORE_WINDOW_EXPAND'), BX.message('JS_CORE_WINDOW_NARROW')]);

            if (this.PARTS.RESIZER)
            {
                this.PARTS.RESIZER.style.display = this.bExpanded ? 'none' : 'block';
            }
        };


        BR.Dialog.prototype.__startResize = function(e)
        {
            if (!this.SETTINGS.resizable)
                return false;

            if(!e) e = window.event;

            this.wndSize = BX.GetWindowScrollPos();
            this.wndSize.innerWidth = BX.GetWindowInnerSize().innerWidth;

            this.pos = BX.pos(this.PARTS.CONTENT_DATA);

            this.x = e.clientX + this.wndSize.scrollLeft;
            this.y = e.clientY + this.wndSize.scrollTop;

            this.dx = this.pos.left + this.pos.width - this.x;
            this.dy = this.pos.top + this.pos.height - this.y;


            // TODO: suspicious
            this.dw = this.pos.width - parseInt(this.PARTS.CONTENT_DATA.style.width) + parseInt(BX.style(this.PARTS.CONTENT, 'padding-right'));

            BX.bind(document, "mousemove", BX.proxy(this.__moveResize, this));
            BX.bind(document, "mouseup", BX.proxy(this.__stopResize, this));

            if(document.body.setCapture)
                document.body.setCapture();

            document.onmousedown = BX.False;

            var b = document.body;
            b.ondrag = b.onselectstart = BX.False;
            b.style.MozUserSelect = this.DIV.style.MozUserSelect = 'none';
            b.style.cursor = 'se-resize';

            BX.onCustomEvent(this, 'onWindowResizeStart');

            return true;
        };

        BR.Dialog.prototype.Resize = function(x, y)
        {
            var new_width = Math.max(x - this.pos.left + this.dx, this.SETTINGS.min_width);
            var new_height = Math.max(y - this.pos.top + this.dy, this.SETTINGS.min_height);

            if (this.SETTINGS.resize_restrict)
            {
                var scrollSize = BX.GetWindowScrollSize();

                if (this.pos.left + new_width > scrollSize.scrollWidth - this.dw)
                    new_width = scrollSize.scrollWidth - this.pos.left - this.dw;
            }

            this.PARTS.CONTENT_DATA.style.width = new_width + 'px';
            this.PARTS.CONTENT_DATA.style.height = new_height + 'px';

            BX.onCustomEvent(this, 'onWindowResize');
            BX.onCustomEvent(this, 'onWindowResizeExt', [{'height': new_height, 'width': new_width}]);
        };

        BR.Dialog.prototype.SetSize = function(obSize)
        {
            this.PARTS.CONTENT_DATA.style.width = obSize.width + 'px';
            this.PARTS.CONTENT_DATA.style.height = obSize.height + 'px';

            BX.onCustomEvent(this, 'onWindowResize');
            BX.onCustomEvent(this, 'onWindowResizeExt', [obSize]);
        };

        BR.Dialog.prototype.setAutosave = function () {
            if (!this.bSetAutosaveDelay)
            {
                this.bSetAutosaveDelay = true;
                setTimeout(BX.proxy(this.setAutosave, this), 10);
            }
        };

        BR.Dialog.prototype.Notify = function(note, bError)
        {
            if (!this.PARTS.NOTIFY)
            {
                this.PARTS.NOTIFY = this.DIV.insertBefore(BX.create('DIV', {
                    props: {className: 'br-warning-block'},
                    children: [
                        BX.create('SPAN', {
                            props: {className: 'br-warning-text'}
                        }),
                        BX.create('SPAN', {
                            props: {className: 'br-warning-icon'}
                        }),
                        BX.create('SPAN', {
                            props: {className: 'br-warning-close'},
                            events: {click: BX.proxy(this.hideNotify, this)}
                        })
                    ]
                }), this.DIV.firstChild);
            }

            if (bError)
                BX.addClass(this.PARTS.NOTIFY, 'br-warning-block-red');
            else
                BX.removeClass(this.PARTS.NOTIFY, 'br-warning-block-green');
            this.PARTS.NOTIFY.style.display = "block";
            this.PARTS.NOTIFY.firstChild.innerHTML = note || '&nbsp;';
            this.PARTS.NOTIFY.firstChild.style.width = (this.PARAMS.width-50) + 'px';
            BX.removeClass(this.PARTS.NOTIFY, 'adm-warning-animate');
        };

        BR.Dialog.prototype.hideNotify = function()
        {
            if(this.PARTS.NOTIFY)
                this.PARTS.NOTIFY.style.display = "none";
        };

        BR.Dialog.prototype.SetContent = function(html)
        {
            var insertData = [];
            if (BX.type.isElementNode(html))
            {
                if (html.parentNode)
                    html.parentNode.removeChild(html);
                this.PARAMS.content = [html];
            }
            else if (BX.type.isString(html))
            {
                html = BX.create('DIV', {html: html});
                for(var i in html.childNodes) {
                    if(html.childNodes[i] instanceof Text) {
                        insertData.push(html.childNodes[i].data);
                    } else {
                        insertData.push(html.childNodes[i]);
                    }
                }
                this.PARAMS.content = insertData;
            }
            else if(html.jquery)
            {
                if(html.length === 1) {
                    this.PARAMS.content = [html[0]];
                } else if(html.length > 1) {
                    html.each(function(){
                        insertData.push(this);
                    });
                    this.PARAMS.content = insertData;
                }

            }

            BX.cleanNode(this.PARTS.CONTENT_DATA);

            BX.adjust(this.PARTS.CONTENT_DATA, {
                children: this.PARAMS.content
            });
        };

        BR.Dialog.prototype.SwapContent = function(cont)
        {
            cont = BX(cont);

            BX.cleanNode(this.PARTS.CONTENT_DATA);
            cont.parentNode.removeChild(cont);
            this.PARTS.CONTENT_DATA.appendChild(cont);
            cont.style.display = 'block';
            this.SetContent(cont.innerHTML);
        };

        // this method deprecated
        BR.Dialog.prototype.adjustSize = function()
        {
        };

        // this method deprecated
        BR.Dialog.prototype.__adjustSize = function()
        {
        };

        BR.Dialog.prototype.adjustSizeEx = function()
        {
            BX.defer(this.__adjustSizeEx, this)();
        };

        BR.Dialog.prototype.__adjustSizeEx = function()
        {
            var ob = this.PARTS.CONTENT_DATA.firstChild,
                new_height = 0,
                new_width = 0,
                window_width = $(document).width(),
                style = window.getComputedStyle ? getComputedStyle(this.DIV, "") : this.DIV.currentStyle,
                popupmargin = parseInt(style.marginLeft) + parseInt(style.marginRight);
            if(!this.PARAMS.width && this.PARAMS.max_width) {
                this.PARTS.CONTENT_DATA.style.width = 'auto';
                this.DIV.style.width = '100%';
            }
            while (ob) // for width
            {
                var ob_width = ob.offsetWidth;
                if(ob_width > new_width)
                    new_width = ob_width;

                ob = BX.nextSibling(ob);
            }
            if(!this.PARAMS.width && this.PARAMS.max_width) {
                this.DIV.style.width = 'auto';
                new_width = Math.min(new_width, this.PARAMS.max_width, window_width - popupmargin);
            }
            if(this.PARAMS.min_width)
                new_width = Math.max(new_width, this.PARAMS.min_width - popupmargin);
            if (new_width)
                this.PARTS.CONTENT_DATA.style.width = new_width + 'px';

            ob = this.PARTS.CONTENT_DATA.firstChild;
            while (ob) // for height
            {
	            var marginTop = parseInt(BX.style(ob, 'margin-top'));
	            var marginBottom = parseInt(BX.style(ob, 'margin-bottom'));

                new_height += ob.offsetHeight;
                if(marginTop)
                	new_height += marginTop;
				if(marginBottom)
					new_height += marginBottom;

                ob = BX.nextSibling(ob);
            }
            new_height = Math.max(new_height, this.PARAMS.min_height);
            if (new_height)
                this.PARTS.CONTENT_DATA.style.height = new_height + 'px';
        };


        BR.Dialog.prototype.__onResizeFinished = function()
        {
            BX.WindowManager.saveWindowSize(
                this.PARAMS.resize_id || this.PARAMS.content_url, {height: parseInt(this.PARTS.CONTENT_DATA.style.height), width: parseInt(this.PARTS.CONTENT_DATA.style.width)}
            );
        };

        BR.Dialog.prototype.Show = function(bNotRegister)
        {
            if ((!this.PARAMS.content) && this.PARAMS.content_url && BX.ajax && !bNotRegister)
            {
                var wait = BX.showWait();

                BX.WindowManager.currently_loaded = this;

                this.CreateOverlay(parseInt(BX.style(wait, 'z-index'))-1);
                this.OVERLAY.style.display = 'block';
                this.OVERLAY.className = 'br-dialog-overlay';

                var post_data = '', method = 'GET';
                if (this.PARAMS.content_post)
                {
                    post_data = this.PARAMS.content_post;
                    method = 'POST';
                }

                var url = this.PARAMS.content_url
                    + (this.PARAMS.content_url.indexOf('?')<0?'?':'&')+'bxsender=' + this._sender;


                BX.ajax({
                    method: method,
                    dataType: 'html',
                    url: url,
                    data: post_data,
                    onsuccess: BX.delegate(function(data) {
                        BX.closeWait(null, wait);

                        this.SetContent(data || '&nbsp;');
                        this.Show();
                    }, this),
                    processScriptsConsecutive: true
                });
            }
            else
            {
                BX.WindowManager.currently_loaded = null;
                BR.Dialog.superclass.Show.apply(this, arguments);
                BX.addClass(this.DIV, 'fadeInDown animated');
                $(this.DIV).one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function(e) {
                    BX.removeClass(this, 'fadeInDown animated');
                });

                this.__adjustSizeEx();
                this.adjustPos(true);

                this.OVERLAY.className = 'br-dialog-overlay';


                BX.addCustomEvent(this, 'onWindowResize', BX.proxy(this.__adjustSizeEx, this));
                if(!this.SETTINGS.draggable){
                    BX.bind(window, 'resize', BX.proxy(this.__adjustSizeEx, this));
                    BX.bind(window, 'resize', BX.proxy(this.adjustPos, this));
                    if(!this.SETTINGS.draggable){
                        //BX.bind(window, 'resize', BX.proxy(this.adjustPos, this));
                        BX.addCustomEvent(this, 'onWindowClose', this.__onWindowClose);
                    }
                }

                if (this.PARAMS.resizable && (this.PARAMS.content_url || this.PARAMS.resize_id))
                    BX.addCustomEvent(this, 'onWindowResizeFinished', BX.delegate(this.__onResizeFinished, this));
                $(this.DIV).trigger('popup.shown');
            }
            if(this.PARAMS.closable && this.PARAMS.overlay_close && !this.SETTINGS.__overlay_event_set){
                this.SetClose(this.OVERLAY);
                this.SETTINGS.__overlay_event_set = true;
            }
        };
        BR.Dialog.prototype.__onWindowClose = function()
        {
            BX.unbind(window, 'resize', BX.proxy(this.adjustPos, this));
        };
        BR.Dialog.prototype.GetInnerPos = function()
        {
            return {'width': parseInt(this.PARTS.CONTENT_DATA.style.width), 'height': parseInt(this.PARTS.CONTENT_DATA.style.height)};
        };

        BR.Dialog.prototype.adjustPos = function(adjustTop)
        {
            adjustTop = (typeof adjustTop === 'undefined' || typeof adjustTop !== 'boolean') ? false : adjustTop;
            if (!this.bExpanded)
            {
                //var windowSize = BX.GetWindowInnerSize();
                // var windowSize = viewSize();
                var doc = docElem( 'clientWidth' );
                var windowSize = { innerWidth: doc.clientWidth, innerHeight: doc.clientHeight };
                var windowScroll = BX.GetWindowScrollPos();

                var style = window.getComputedStyle ? getComputedStyle(this.DIV, "") : elem.currentStyle;

                var width = parseInt(this.DIV.offsetWidth) + parseInt(style.marginLeft) + parseInt(style.marginRight);
                var height = parseInt(this.DIV.offsetHeight) + parseInt(style.marginTop) + parseInt(style.marginBottom);

                var style = {
                    left: Math.max(0, parseInt(windowScroll.scrollLeft + (windowSize.innerWidth / 2 - width / 2))) + 'px'
                };
                if(adjustTop)
                    style.top = Math.max(0, parseInt(windowScroll.scrollTop + Math.max(0, windowSize.innerHeight / 2 - height / 2))) + 'px';
                BX.adjust(this.DIV, { style: style });
            }
        };

        BR.Dialog.prototype.GetContent = function () {return this.PARTS.CONTENT_DATA};

        BR.Dialog.prototype.btnSave = BR.Dialog.btnSave = {
            title: BX.message('JS_CORE_WINDOW_SAVE'),
            id: 'savebtn',
            name: 'savebtn',
            className: BX.browser.IsIE() && BX.browser.IsDoctype() && !BX.browser.IsIE10() ? '' : 'adm-btn-save',
            action: function () {
                this.disableUntilError();
                this.parentWindow.PostParameters();
            }
        };

        BR.Dialog.prototype.btnCancel = BR.Dialog.btnCancel = {
            title: BX.message('JS_CORE_WINDOW_CANCEL'),
            id: 'cancel',
            name: 'cancel',
            action: function () {
                this.parentWindow.Close();
            }
        };

        BR.Dialog.prototype.btnClose = BR.Dialog.btnClose = {
            title: BX.message('JS_CORE_WINDOW_CLOSE'),
            id: 'close',
            name: 'close',
            action: function () {
                this.parentWindow.Close();
            }
        };

        BR.ClosePopup = function()
        {
            BX.WindowManager.Get().Close();
        };

        BR.WindowLink = function(params) {
            BR.WindowLink.superclass.constructor.apply(this, arguments);
        };
        BX.extend(BR.WindowLink, BX.CWindowButton);
        BR.WindowLink.prototype.Button = function(parentWindow)
        {
            this.parentWindow = parentWindow;

            var btn = {
                props: {
                    'className': 'btn2',
                    'name': this.id ? this.id : this.name,
                    'id': this.id
                },
                'html': this.title ? this.title : this.name,
            };

            if (this.hint)
                btn.props.title = this.hint;
            if (!!this.className)
                btn.props.className = this.className;

            if (this.action)
            {
                btn.events = {
                    'click': BX.delegate(this.action, this)
                };
            }
            else if (this.onclick)
            {
                if (BX.browser.IsIE())
                {
                    btn.events = {
                        'click': BX.delegate(function() {eval(this.onclick)}, this)
                    };
                }
                else
                {
                    btn.attrs = {
                        'onclick': this.onclick
                    };
                }
            }

            this.btn = BX.create('A', btn);

            return this.btn;
        };
    }
    BR.GetFormParams = function(form)
    {
        var data = {};
        var i, s = "";
        var n = form.elements.length;
        var elname;
        for(i=0; i<n; i++)
        {
            var el = form.elements[i];
            if (el.disabled)
                continue;
            var arEl=false;
            elname = el.name;
            if(el.name.indexOf("[]") == el.name.length-2)
            {
                elname = elname.substr(0, el.name.length-2);
                arEl=true;
                if(typeof data[elname] == 'undefined')
                    data[elname] = [];
            }
            switch(el.type.toLowerCase())
            {
                case 'checkbox':
                case 'radio':
                    if(el.checked)
                    {
                        if(arEl)
                        {
                            data[elname].push(el.value);
                        } else {
                            data[elname] = el.value;
                        }
                    }
                    break;
                case 'select-multiple':
                    var j, bAdded = false;
                    var l = el.options.length;
                    data[el.name] = [];
                    for (j=0; j<l; j++)
                    {
                        if (el.options[j].selected)
                        {
                            data[el.name].push(el.value);
                        }
                    }
                    break;
                case 'select-one':
                case 'text':
                case 'textarea':
                case 'password':
                case 'hidden':
                default:
                    if(arEl)
                    {
                        data[elname].push(el.value);
                    } else {
                        data[elname] = el.value;
                    }
                    break;
            }
        }
        return data;
    };

    var postponedGoals = [];
    if(typeof window.yaReach != 'undefined'){
        postponedGoals = window.yaReach;
    }
    window.yaReach = {
        'yaReachTO': false,
        'push': function(goal){
            if(typeof window.yaCounter23311864 == 'undefined')
            {
                postponedGoals.push(goal);
            } else {
                window.yaCounter23311864.reachGoal(goal);
            }
        },
        'reachPostponed': function()
        {
            window.yaReach.yaReachTO=false;
            if(typeof window.yaCounter23311864 == 'undefined')
            {
                window.yaReach.yaReachTO = setTimeout(window.yaReach.reachPostponed, 500);
            } else {
                for(var i in postponedGoals)
                {
                    window.yaCounter23311864.reachGoal(postponedGoals[i]);
                }
                postponedGoals = [];
            }
        }
    };
    window.yaReach.yaReachTO = setTimeout(window.yaReach.reachPostponed, 500);

    //window.JSONRPC = function(options) {
    //    var self = this,
    //        params = options;
    //    self.RPC_CALL_ID = 1;
    //    self.call = function(method, args, success, callback_data)
    //    {
    //        var requestOptions = {
    //            method: undefined,
    //            arguments: {},
    //            success: undefined,
    //            complete: undefined,
    //            error: undefined,
    //            callback_data: []
    //        }
    //        if(typeof method == "object")
    //        {
    //            requestOptions = $.extend(requestOptions, method);
    //        } else {
    //            if(args == undefined) args = {};
    //            if(callback_data == undefined) callback_data = [];
    //
    //            requestOptions.method = method;
    //            requestOptions.arguments = args;
    //            requestOptions.success = success;
    //            requestOptions.callback_data = callback_data;
    //        }
    //        if(requestOptions.method) {
    //            var callParams = {
    //                "jsonrpc": "2.0",
    //                "method": requestOptions.method,
    //                "params": requestOptions.arguments,
    //                "id": self.RPC_CALL_ID++
    //            };
    //            $.ajax({
    //                type: 'post',
    //                url: params.rpcUrl,
    //                data: JSON.stringify(callParams),
    //                success: function(data)
    //                {
    //                    if(data.error) {
    //                        if (requestOptions.error)
    //                            requestOptions.error.apply(self, [data.error].concat(requestOptions.callback_data));
    //                    } else {
    //                        if (requestOptions.success)
    //                            requestOptions.success.apply(self, [data.result].concat(requestOptions.callback_data));
    //                    }
    //                    if (requestOptions.complete)
    //                        requestOptions.complete.apply(self, [data].concat(requestOptions.callback_data));
    //                },
    //                error: function(data)
    //                {
    //                    if (requestOptions.error)
    //                        requestOptions.error.apply(self, [data.error].concat(requestOptions.callback_data));
    //                    if (requestOptions.complete)
    //                        requestOptions.complete.apply(self, [data].concat(requestOptions.callback_data));
    //                }
    //            });
    //        }
    //    };
    //};

    /* OPTIONS:
     * class: класс,
     * listeners (optional): {
     *   success,
     *   complete,
     *   error
     * },
     * url (optional)
     * */
    window.JSONRPC = function(options) {
        this.cl = "";
        this.listeners = {
            success: undefined,
            error: undefined,
            complete: undefined
        };
        $.extend(this, options);
        this.call_id = 1;
    };

    JSONRPC.prototype.url = "/api/ajax.php";

    JSONRPC.prototype.abort = function(){
        if(this.last_xhr && this.last_xhr.__abortable)
            this.last_xhr.abort();
        return this;
    }
    JSONRPC.prototype.send = function(method, arguments, listeners, callback_data, abortable){
        var self = this,
            funcs,
            callParams = {
                jsonrpc: "2.0",
                method: "",
                params: arguments || {},
                id: this.call_id++,
                callback_data: callback_data,
                abortable: true
            };

        if(typeof abortable == "undefined"){
            abortable = true;
        }
        if(typeof method == "object"){
            if(this.cl)
                method.method = this.cl + "." + method.method;
            funcs = $.extend({}, this.listeners, method.listeners);
            delete method.listeners;
            $.extend(callParams, method);
        }else{
            if(this.cl)
                callParams.method = this.cl + ".";
            callParams.method += method;
            callParams.abortable = abortable;

            funcs = $.extend({}, this.listeners, listeners);
        }
        callParams.params["bitrix_sessid"] = BX.bitrix_sessid();
        callParams = JSON.stringify(callParams);
        var url = self.url;
        // remove F**KING #
        if(url.indexOf('#') >= 0)
            url = url.substr(0, url.indexOf('#'));
        url += (url.indexOf('?') < 0 ? '?' : '&') + 'jsonrpc=Y';
        this.last_xhr = $.ajax({
            type: 'POST',
            url: url,
            dataType: "json",
            data: callParams,
            success: function(data){
                //if(data.bitrix_sessid)
                //    phpVars.bitrix_sessid = data.bitrix_sessid;
                if (data.error){
                    if(funcs.error)
                        funcs.error(data.error, callback_data);
                }else{
                    if(funcs.success)
                        funcs.success(data.result, callback_data);
                }
            },
            error: function(data, status, error) {
                if(status !== 'abort') {
                    if (funcs.error) {
                        funcs.error(error, callback_data);
                    }
                }
            },
            complete: function(xhr, status){
                if (funcs.complete){
                    funcs.complete(xhr, status, callback_data);
                }
            }
        });
        this.last_xhr.__abortable = abortable;
    };
    var actionRPC = new JSONRPC({ cl: "Other" });
    window.sendAction = BR.sendAction = function(type, action, data) {
        if(typeof data == 'undefined')
            data = [];
        actionRPC.send('event', {
            'type': type,
            'action': action,
            'data': data
        })
    };
    BR.sendOther = function(func, data) {
        if(typeof data == 'undefined')
            data = {};
        actionRPC.send(func, data);
    };

    BR.arrayToObject = function(array){
        var obj = {};
        for(var i in array) {
            if(array[i].name.substr(-2) == "[]") {
                var name = array[i].name.substr(0, array[i].name.length-2);
                if(!obj[name])
                    obj[name] = [];
                obj[name].push(array[i].value);
            } else
                obj[array[i].name] = array[i].value;
        }
        return obj;
    };
    BR.escapeHtml = function(text) {
        var map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };

        return text.replace(/[&<>"']/g, function(m) { return map[m]; });
    };
    BR.unescapeHtml = function(text) {
        var map = {
            '&amp;':  '&',
            '&lt;':   '<',
            '&gt;':   '>',
            '&quot;': '"',
            '&#039;': "'"
        };

        return text.replace(/&(?:amp|[lg]t|quot|#039);/g, function(m) { return map[m]; });
    };
    BR.isValidEmail = function(emailAddress) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailAddress);
    };
    BR.addUrlParam = function(url, param, value) {
        return url + (url.indexOf('?') > -1 ? '&' : '?') + param + '=' + value;
    };
    // BR.preloadImages = function(imagelist) {
    //     if(!imagelist.length) return;
    //     imagelist.forEach(function(image){
    //         $("<img src='"+ image +"'/>");
    //     });
    // };

    // $isMobile=false;
    // //isWidthLimited = $(window).width() <= 1024;
    // if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    //     $isMobile=true;
    // }
    function mobilecheck() {
        var check = false;
        (function(a){if(/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
        return check;
    }
    window.mobilecheck = mobilecheck;
    window.isMobile = mobilecheck();

    // iOS 100vh fix
    if(window.isMobile) {
        document.getElementsByTagName('html')[0].className += ' is-mobile';
    }

})(jQuery, document, window);
function docElem( property )
{
    var t;
    return ((t = document.documentElement) || (t = document.body.parentNode)) && BX.util.isNumber( t[property] ) ? t : document.body;
}
// Returns object({innerWidth, innerHeight}) with inner width and height of the window excluding scrollbars, if visible
function viewSize()
{
    // Shelby Moore https://bugzilla.mozilla.org/show_bug.cgi?id=156388#c14
    // This algorithm avoids creating test page to determine if document.documentElement.client[Width|Height] is greater then view size,
    // will succeed where such test page wouldn't detect dynamic unreliability,
    // and will only fail in the case the right or bottom edge is within the width of a scrollbar from edge of the viewport that has visible scrollbar(s).
    var doc = docElem( 'clientWidth' ),
        body = document.body,
        w, h;
    return BX.util.isNumber( document.clientWidth ) ? { innerWidth : document.clientWidth, innerHeight : document.clientHeight } :
        doc === body
        || (w = Math.max( doc.clientWidth, body.clientWidth )) > self.innerWidth
        || (h = Math.max( doc.clientHeight, body.clientHeight )) > self.innerHeight ? { innerWidth : body.clientWidth, innerHeight : body.clientHeight } :
        { innerWidth : w, innerHeight : h };
}

// ImageLoader
(function() {
    function ImageLoader(images, cb) {
        this.images = [];
        this.loadingImages = [];
        this.callbacks = [];
        this.loadedImages = 0;

        if(images) {
            if(BX.util.isString(images)) {
                images = [images];
            }

            this.images = images;
            if(cb)
                this.callbacks.push(cb);
            this.start();
        }
    }
    ImageLoader.prototype.start = function() {
        var self = this;
        setTimeout(function() {
            self._start();
        }, 1);
    };
    ImageLoader.prototype._start = function() {
        var self = this;
        for(var i in this.images) {
            if(!this.loadingImages[i]) {
                this.loadingImages[i] = true;
                var img = new Image();
                img.src = this.images[i];
                img.onload = function () {
                    self.handleImageLoaded();
                };
            }
        }
    };
    ImageLoader.prototype.handleImageLoaded = function() {
        this.loadedImages++;
        if(this.loadedImages == this.images.length) {
            // call all cb only once
            for(var i in this.callbacks) {
                this.callbacks[i]();
            }
            this.callbacks = [];
        }
    };
    ImageLoader.prototype.addImage = function(image, cb) {
        this.images.push(image);
        if(cb) this.callbacks.push(cb);
        this.start();
    };

    BR.preloadImages = function(images, cb) {
        return new ImageLoader(images, cb);
    };
    BR.formDataObject = {
        to: function(source) {
            return Object.keys(source).reduce(function (output, key) {
                var parentKey = key.match(/[^\[]*/i);
                var paths = key.match(/\[.*?\]/g) || [];
                paths = [parentKey[0]].concat(paths).map(function (key) {
                    return key.replace(/\[|\]/g, '');
                });
                var currentPath = output;
                while (paths.length) {
                    var pathKey = paths.shift();

                    if (pathKey in currentPath) {
                        currentPath = currentPath[pathKey];
                    } else {
                        if (pathKey === "") {
                            if ($.isArray(source[key])) {
                                source[key].forEach(function(val) { currentPath.push(val); });
                            } else {
                                currentPath.push(source[key]);
                            }
                        } else {
                            currentPath[pathKey] = paths.length ? isNaN(paths[0]) && paths[0] !== "" ? {} : [] : source[key];
                        }
                        currentPath = currentPath[pathKey];
                    }
                }

                return output;
            }, {});
        },
        from: function(obj) {
            function recur(newObj, propName, currVal) {
                if (Array.isArray(currVal) || Object.prototype.toString.call(currVal) === '[object Object]') {
                    Object.keys(currVal).forEach(function(v) {
                        recur(newObj, propName + "[" + v + "]", currVal[v]);
                    });
                    return newObj;
                }

                newObj[propName] = currVal;
                return newObj;
            }

            var keys = Object.keys(obj);
            return keys.reduce(function(newObj, propName) {
                return recur(newObj, propName, obj[propName]);
            }, {});
        }
    };
    $.fn.serializeFormJSON = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (typeof o[this.name] !== 'undefined') {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return BR.formDataObject.to(o);
    };
})();

jQuery(function($) {
    $('a.scroll').click(function(e) {
        e.preventDefault();
        var el = $($(this).attr('href'));
        if(el.length) {
            var h = el.offset().top - 50;
            $('html, body').animate({
                scrollTop: h
            }, 300);
        }
    });
});



function validateForm(event) {
    // fetch cross-browser event object and form node
    event = (event ? event : window.event);
    var
        form = (event.target ? event.target : event.srcElement),
        f, field, message_shown = false, formvalid = true;

    // loop all fields
    for (f = 0; f < form.elements.length; f++) {

        // get field
        field = form.elements[f];

        // ignore buttons, fieldsets, etc.
        if (field.nodeName !== "INPUT" && field.nodeName !== "TEXTAREA" && field.nodeName !== "SELECT") continue;

        // is native browser validation available?
        if (typeof field.willValidate !== "undefined") {

            // native validation available
            if (field.nodeName === "INPUT" && field.type !== field.getAttribute("type")) {

                // input type not supported! Use legacy JavaScript validation
                field.setCustomValidity(LegacyValidation(field) ? "" : "error");

            }

            // native browser check
            field.checkValidity();

        }
        else {

            // native validation not available
            field.validity = field.validity || {};

            // set to result of validation function
            field.validity.valid = LegacyValidation(field);

            // if "invalid" events are required, trigger it here

        }

        hideErrorMessages($(field));
        if (field.validity.valid) {
            // remove error styles and messages
            $(field).removeClass('check-error')
                .addClass('check-valid')
                .trigger('field-valid');

        }
        else {
            // style field, show error, etc.
            $(field).addClass('check-error')
                .removeClass('check-valid')
                .trigger('field-invalid');
            if (!message_shown) {
                var message = $(field).data('error-text') || "Сначала заполни это поле :-)",
                    $element;
                if ($(field).data('error-element')) {
                    $element = $($(field).data('error-element'));
                }
                if (!$element || !$element.length) {
                    $element = $(field);
                }
                // show error popover
                showErrorMessage($element, message);
                message_shown = true;
            }

            // form is invalid
            formvalid = false;
        }

    }

    // cancel form submit if validation fails
    if (!formvalid) {
        if (event.preventDefault) event.preventDefault();
    }
    return formvalid;
}
function hideErrorMessages($fields) {
    $fields.each(function() {
        var $field = $(this);
        if ($field.not(':visible')) {
            $field = $field.closest(':visible');
        }
        var timer = $field.data('$error_message_box_timer'),
            $message_box = $field.data('$error_message_box');
        if (timer) {
            clearTimeout(timer);
            $field.removeData('$error_message_box_timer');
        }

        if ($message_box && $message_box.length) {
            $message_box.fadeOut('fast', function(){$message_box.remove();});
            $field.removeData('$error_message_box');
        }
    });
}
function showErrorMessage($field, message) {
    if ($field.not(':visible')) {
        $field = $field.closest(':visible');
    }
    var fieldOffset = $field.offset();
    var fieldSizes = {
        width: $field.width(),
        height: $field.outerHeight()
    };
    var $message_box = $('<div class="error-message"></div>').html(message).css({
        position: 'absolute',
    }).appendTo($(document.body));
    $message_box.css({
        top: fieldOffset.top + fieldSizes.height + 12,
        left: fieldOffset.left + Math.floor(fieldSizes.width / 2) - ($message_box.width() / 2),
    }).hide().fadeIn('fast');

    hideErrorMessages($field);

    $field.data('$error_message_box', $message_box);
    $field.data('$error_message_box_timer', setTimeout(hideErrorMessages.bind(undefined, $field), 5000));
}
// basic legacy validation checking
function LegacyValidation(field) {

    var
        valid = true,
        val = field.value,
        type = field.getAttribute("type"),
        chkbox = (type === "checkbox" || type === "radio"),
        required = field.getAttribute("required"),
        minlength = field.getAttribute("minlength"),
        maxlength = field.getAttribute("maxlength"),
        pattern = field.getAttribute("pattern");

    // disabled fields should not be validated
    if (field.disabled) return valid;

    // value required?
    valid = valid && (!required ||
            (chkbox && field.checked) ||
            (!chkbox && val !== "")
        );

    // minlength or maxlength set?
    valid = valid && (chkbox || (
            (!minlength || val.length >= minlength) &&
            (!maxlength || val.length <= maxlength)
        ));

    // test pattern
    if (valid && pattern) {
        pattern = new RegExp(pattern);
        valid = pattern.test(val);
    }

    return valid;
}